# show upload dialog
from google.colab import files
uploaded = files.upload()

# read data
import pandas as pd
import io
data = pd.read_csv(io.StringIO(uploaded['beer.csv'].decode('utf-8')), header=-1)
data.head()