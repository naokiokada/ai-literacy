# -*- coding: utf-8 -*-

try:
    import sklearn
    sklearnExists = True
except ImportError:
    sklearnExists = False

try:
    import numpy
    numpyExists = True
except ImportError:
    numpyExists = False

try:
    import scipy
    scipyExists = True
except ImportError:
    scipyExists = False

try:
    import matplotlib
    matplotlibExists = True
except ImportError:
    matplotlibExists = False

try:
    from PIL import Image
    PILExists = True
except ImportError:
    PILExists = False

try:
    import keras
    kerasExists = True
except ImportError:
    kerasExists = False

if sklearnExists == True:
    if(hasattr(sklearn, '__version__')):
        print('scikit-learnのバージョンは{0}です'.format(sklearn.__version__))
    else:
        print('scikit-learnのバージョンは不明です')
else:
    print('scikit-learnはインストールされていません')

if numpyExists == True:
    if(hasattr(numpy, '__version__')):
        print('numpyのバージョンは{0}です'.format(numpy.__version__))
    else:
        print('numpyのバージョンは不明です')
else:
    print('numpyはインストールされていません')

if scipyExists == True:
    if(hasattr(scipy, '__version__')):
        print('scipyのバージョンは{0}です'.format(scipy.__version__))
    else:
        print('scipyのバージョンは不明です')
else:
    print('scypyはインストールされていません')

if matplotlibExists == True:
    if(hasattr(matplotlib, '__version__')):
        print('matplotlibのバージョンは{0}です'.format(matplotlib.__version__))
    else:
        print('matplotlibのバージョンは不明です')
else:
    print('matplotlibはインストールされていません')

if PILExists == True:
    if(hasattr(Image, 'PILLOW_VERSION')):
        print('PIL(Pillow)のバージョンは{0}です'.format(Image.PILLOW_VERSION))
    else:
        print('PIL(Pillow)のバージョンは{0}です'.format(Image.__version__))
else:
    print('PIL(Pillow)はインストールされていません')

if kerasExists == True:
    if(hasattr(keras, '__version__')):
        print('kerasのバージョンは{0}です'.format(keras.__version__))
    else:
        print('kerasのバージョンは不明です')
else:
    print('kerasがインストールされていないか、まだ設定が済んでいません')
